#include <stdlib.h>
#include <time.h>
#include <QApplication>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    if (w.aiLib()->isLoaded()) {
        srand(time(NULL));
        w.show();
        return a.exec();
    } else {
        return 1;
    }
}
