#include "graphicscarditem.h"

GraphicsCardItem::GraphicsCardItem() :
    QGraphicsPixmapItem()
{
}

GraphicsCardItem::GraphicsCardItem(MainWindow* mainWindow, Card * card, QPixmap pixmapFront, QPixmap pixmapBack) :
    QGraphicsPixmapItem(pixmapBack)   // By defaults the card is closed.
{
    fMainWindow = mainWindow;
    fCard = card;
    fPixmapFront = pixmapFront;
    fPixmapBack = pixmapBack;
    setSelected(false);
    setClosed(true);                  // Same here, the card is closed.
}

GraphicsCardItem::~GraphicsCardItem()
{
    delete fCard;
}

Card* GraphicsCardItem::card() const
{
    return fCard;
}

bool GraphicsCardItem::isSelected() const
{
    return fIsSelected;
}

bool GraphicsCardItem::isClosed() const
{
    return fIsClosed;
}

void GraphicsCardItem::setSelected(bool selected)
{
    fIsSelected = selected;
}

void GraphicsCardItem::setClosed(bool closed)
{
    if (closed != fIsClosed) {
        fIsClosed = closed;
        if (fIsClosed) {
            setPixmap(fPixmapBack);
        } else {
            setPixmap(fPixmapFront);
        }
    }
}

void GraphicsCardItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    fMainWindow->onGraphicsCardItemClicked(this);
}
