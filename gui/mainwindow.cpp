#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    fScene = new QGraphicsScene(0, 0, POSITION_SCENE_WIDTH, POSITION_SCENE_HEIGHT);
    ui->graphicsView->setScene(fScene);

    // Load the AI library.
    fAiLib = new QLibrary("ai.dll");
    if (!fAiLib->load()) {
        QMessageBox::information(NULL, "Oops!", "Could not load ai.dll", QMessageBox::Ok);
        return;
    }
    fDecideFuncPtr = (ai_lib_decide)fAiLib->resolve("decide");
    if (!fDecideFuncPtr) {
        QMessageBox::information(NULL, "Oops!", "Could not load function decide()", QMessageBox::Ok);
        fAiLib->unload();
        return;
    }

    // Do some connects.
    connect(ui->actionNewGame, SIGNAL(triggered(bool)), this, SLOT(actionNewGame_triggered()));
    connect(ui->actionExit, SIGNAL(triggered(bool)), this, SLOT(actionExit_triggered()));
    connect(ui->actionAbout, SIGNAL(triggered(bool)), this, SLOT(actionAbout_triggered()));
    connect(ui->btnClaimRank, SIGNAL(clicked(bool)), this, SLOT(btnClaimRank_clicked()));
    connect(ui->btnBelieve, SIGNAL(clicked(bool)), this, SLOT(btnBelieve_clicked()));
    connect(ui->btnBullshit, SIGNAL(clicked(bool)), this, SLOT(btnBullshit_clicked()));

    // Create players.
    for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
        fPlayers.append(new Player(i, i != 0)); // Only one human player.
    }

    gameEnded(false);
}

MainWindow::~MainWindow()
{
    if (fAiLib->isLoaded()) {
        fAiLib->unload();
    }
    delete fAiLib;
    delete fScene;
    delete ui;
}

QLibrary* MainWindow::aiLib() const
{
    return fAiLib;
}

void MainWindow::onGraphicsCardItemClicked(GraphicsCardItem * item)
{
    Card * card = item->card();

    // If it's a closed card, or the card can't be selected, or it's not the human's turn - exit.
    if (item->isClosed() || card == NULL ||
        card->state == CARD_STATE_IN_GAME || card->state == CARD_STATE_OUT ||
        fCurrentPlayerIndex != getHumanPlayer()->number()) {
        return;
    }

    // Change the selection state.
    qreal y = item->y();
    if (item->isSelected()) {
        item->setY(y + POSITION_SELECTED_DELTA);
        item->setSelected(false);
    } else {
        item->setY(y - POSITION_SELECTED_DELTA);
        item->setSelected(true);
    }
}

void MainWindow::actionNewGame_triggered()
{
    ui->textBrowserLog->clear();
    dealCards();
    fCurrentPlayerIndex = -1;   // rand() % fPlayers.size();
    nextRound(true);
}

void MainWindow::actionExit_triggered()
{
    close();
}

void MainWindow::actionAbout_triggered()
{
    QMessageBox::information(NULL, "About", "by Valeriy Streltsov vostreltsov@gmail.com", QMessageBox::Ok);
}

void MainWindow::btnClaimRank_clicked()
{
    int selectedRow = ui->lwRank->currentRow();
    if (selectedRow == -1) {
        QMessageBox::information(NULL, "Oops!", "You should select a rank to claim", QMessageBox::Ok);
        return;
    }

    // The human player is claiming card rank.
    Player* player = getHumanPlayer();

    // Get the new rank and selected cards.
    int rank = 6 + selectedRow;
    QVector<GraphicsCardItem *> selected = player->selectedCards();

    if (selected.isEmpty()) {
        QMessageBox::information(NULL, "Oops!", "You should select at least one card", QMessageBox::Ok);
        return;
    }

    doClaimRank(player, selected, rank);
}

void MainWindow::btnBelieve_clicked()
{
    // The human player believes the previous player.
    // If he makes a mistake, he takes all cards. If not, all cards in game are thrown out.
    doBelieve(getHumanPlayer());
}

void MainWindow::btnBullshit_clicked()
{
    // The human player doesn't believe the previous player.
    // If he makes a mistake, he takes all cards. If not, all cards in game are thrown out.
    doBullshit(getHumanPlayer());
}

int MainWindow::nextPlayerIndex(int currentPlayer) const
{
    int result = currentPlayer;
    do {
        result++;
        if (result == fPlayers.size()) {
            result = 0;
        }
        // If we reached an active player - return his index.
        if (!fPlayers[result]->isOut()) {
            return result;
        }
    } while (result != currentPlayer);
    return -1;
}

int MainWindow::previousPlayerIndex(int currentPlayer) const
{
    int result = currentPlayer;
    do {
        result--;
        if (result < 0) {
            result = fPlayers.size() - 1;
        }
        // If we reached an active player - return his index.
        if (!fPlayers[result]->isOut()) {
            return result;
        }
    } while (result != currentPlayer);
    return -1;
}

Player* MainWindow::getHumanPlayer() const
{
    foreach (Player * player, fPlayers) {
        if (!player->isAi()) {
            return player;
        }
    }
    return NULL;
}

QVector<Player *> MainWindow::getAiPlayers() const
{
    QVector<Player *> result;
    foreach (Player * player, fPlayers) {
        if (player->isAi()) {
            result.append(player);
        }
    }
    return result;
}

QVector<Player *> MainWindow::getActivePlayers(bool active) const
{
    QVector<Player *> result;
    foreach (Player * player, fPlayers) {
        if (player->isOut() != active) {
            result.append(player);
        }
    }
    return result;
}

QVector<GraphicsCardItem*> MainWindow::getCardsByState(CardState state) const
{
    QVector<GraphicsCardItem*> result;
    foreach (GraphicsCardItem* card, fCards) {
        if (card->card()->state == state) {
            result.append(card);
        }
    }
    return result;
}

QVector<GraphicsCardItem*> MainWindow::findCardsOfRank(QVector<GraphicsCardItem *> cards, int rank) const
{
    QVector<GraphicsCardItem*> result;
    foreach (GraphicsCardItem* card, cards) {
        if (card->card()->rank == rank) {
            result.append(card);
        }
    }
    return result;
}

QPixmap MainWindow::getCardPixmap(Card* card)
{
    QString name = ":/cards/images/";
    if (card == NULL) {
        name += "back.png";
    } else {
        if (card->suit == CARD_SUIT_SPADES) {
            name += "spades";
        } else if (card->suit == CARD_SUIT_HEARTS) {
            name += "hearts";
        } else if (card->suit == CARD_SUIT_DIAMONDS) {
            name += "diamonds";
        } else if (card->suit == CARD_SUIT_CLUBS) {
            name += "clubs";
        }
        name += QString::number(card->rank) + ".png";
    }
    return QPixmap::fromImage(QImage(name));
}

void MainWindow::setCardsPositions()
{
    // Deselect all cards.
    foreach (GraphicsCardItem* card, fCards) {
        card->setSelected(false);
    }

    int deltaX, x, y, curZValue;
    // Set positions for the human player's cards (horizontally).
    Player * human = getHumanPlayer();
    if (!human->cards().isEmpty()) {
        deltaX = fScene->width() / human->cards().size();
        x = 0;
        y = POSITION_Y_BOTTOM;
        curZValue = 0;
        foreach (GraphicsCardItem * item, human->cards()) {
            item->setPos(x, y);
            item->setZValue(curZValue++);
            x += deltaX;
        }
    }

    // Set positions for the ai players' cards (vertically).
    deltaX = fScene->width() / (2 * getAiPlayers().size());
    x = POSITION_X_LEFT;
    foreach (Player * aiPlayer, getAiPlayers()) {
        if (!aiPlayer->cards().isEmpty()) {
            int deltaY = (fScene->height() - 2 * aiPlayer->cards().first()->pixmap().height()) / aiPlayer->cards().size();
            y = 0;
            curZValue = 0;
            foreach (GraphicsCardItem * item, aiPlayer->cards()) {
                item->setPos(x, y);
                item->setZValue(curZValue++);
                y += deltaY;
            }
        }
        x += deltaX;
    }

    // Set positions for the out cards.
    int ololX = x;
    QVector<GraphicsCardItem*> outCards = getCardsByState(CARD_STATE_OUT);
    if (!outCards.isEmpty()) {
        deltaX = (fScene->width() - x) / outCards.size();
        y = 0;
        curZValue = 0;
        foreach (GraphicsCardItem * item, outCards) {
            item->setPos(x, y);
            item->setZValue(curZValue++);
            x += deltaX;
        }
    }

    // Set positions for the cards in game.
    x = ololX;
    QVector<GraphicsCardItem*> cardsInGame = getCardsByState(CARD_STATE_IN_GAME);
    if (!cardsInGame.isEmpty()) {
        deltaX = (fScene->width() - x) / cardsInGame.size();
        y = fScene->height() / 3;
        curZValue = 0;
        foreach (GraphicsCardItem * item, cardsInGame) {
            item->setPos(x, y);
            item->setZValue(curZValue++);
            x += deltaX;
        }
    }
}

void MainWindow::dealCards()
{
    foreach (GraphicsCardItem * card, fCards) {
        delete card;
    }
    fScene->clear();
    fCards.clear();

    // Create cards.
    for (int i = RANK_MIN; i <= RANK_MAX; i++) {
        Card * card = new Card(CARD_STATE_ON_HANDS, CARD_SUIT_SPADES, i, -1);
        fCards.append(new GraphicsCardItem(this, card, getCardPixmap(card), getCardPixmap(NULL)));

        card = new Card(CARD_STATE_ON_HANDS, CARD_SUIT_HEARTS, i, -1);
        fCards.append(new GraphicsCardItem(this, card, getCardPixmap(card), getCardPixmap(NULL)));

        card = new Card(CARD_STATE_ON_HANDS, CARD_SUIT_DIAMONDS, i, -1);
        fCards.append(new GraphicsCardItem(this, card, getCardPixmap(card), getCardPixmap(NULL)));

        card = new Card(CARD_STATE_ON_HANDS, CARD_SUIT_CLUBS, i, -1);
        fCards.append(new GraphicsCardItem(this, card, getCardPixmap(card), getCardPixmap(NULL)));
    }

    // Randomize them.
    for (int i = 0; i < fCards.size(); i++) {
        int index1 = rand() % fCards.size();
        int index2 = rand() % fCards.size();
        GraphicsCardItem * tmp = fCards[index1];
        fCards[index1] = fCards[index2];
        fCards[index2] = tmp;
    }

    // Pass the cards to all players.
    for (int i = 0; i < fPlayers.size(); i++) {
        fPlayers[i]->clearCards();
    }
    int curPlayer = 0;
    foreach (GraphicsCardItem* card, fCards) {
        fPlayers[curPlayer]->addCard(card, !fPlayers[curPlayer]->isAi());  // Number of the card is updated inside addCard().
        curPlayer++;
        if (curPlayer == fPlayers.size()) {
            curPlayer = 0;
        }
    }

    // Remove 4 of kind for all players.
    foreach (Player * player, fPlayers) {
        for (int i = RANK_MIN; i <= RANK_MAX; i++) {
            QVector<GraphicsCardItem*> nOfKind = findCardsOfRank(player->cards(), i);
            if (nOfKind.size() == 4) {
                player->removeCards(nOfKind, CARD_STATE_OUT, true);
            }
        }
    }

    // Add cards to the scene.
    foreach (GraphicsCardItem * card, fCards) {
        fScene->addItem(card);
    }

    // Set positions of the cards.
    setCardsPositions();
    ui->textBrowserLog->append("Cards are dealt");
}

void MainWindow::openCardsInGame()
{
    QVector<GraphicsCardItem*> cards = getCardsByState(CARD_STATE_IN_GAME);
    foreach (GraphicsCardItem* card, cards) {
        card->setClosed(false);
    }
    setCardsPositions();
}

void MainWindow::throwCardsOut(QVector<GraphicsCardItem*> cards)
{
    foreach (GraphicsCardItem* card, cards) {
        card->card()->owner = -1;
        card->card()->state = CARD_STATE_OUT;
        card->setClosed(false);
    }
    setCardsPositions();
}

bool MainWindow::doCheck(Player * player, DecisionCode decision)
{
    bool bullshit = false;
    QVector<GraphicsCardItem*> cardsInGame = getCardsByState(CARD_STATE_IN_GAME);
    foreach (GraphicsCardItem* card, cardsInGame) {
        bullshit |= (card->card()->rank != fCurrentRank);
    }

    bool guessRight = (bullshit && decision == DECISION_CODE_BULLSHIT) || (!bullshit && decision != DECISION_CODE_BULLSHIT);

    if (guessRight) {
        // The player was right.
        if (decision == DECISION_CODE_BELIEVE) {
            // Cards go out.
            ui->textBrowserLog->append("Player " + QString::number(player->number() + 1) + " says: \"I believe you!\" - RIGHT!");
            throwCardsOut(cardsInGame);
        } else {
            // Previous player takes all cards in game.
            ui->textBrowserLog->append("Player " + QString::number(player->number() + 1) + " says: \"Bullshit!\" - RIGHT!");
            Player* prevPlayer = fPlayers[previousPlayerIndex(fCurrentPlayerIndex)];
            prevPlayer->addCards(cardsInGame, !prevPlayer->isAi());
        }
    } else {
        // The player made a mistake.
        if (decision == DECISION_CODE_BELIEVE) {
            ui->textBrowserLog->append("Player " + QString::number(player->number() + 1) + " says: \"I believe you!\" - WRONG!");
        } else {
            ui->textBrowserLog->append("Player " + QString::number(player->number() + 1) + " says: \"Bullshit!\" - WRONG!");
        }
        // Current player takes all cards anyways.
        player->addCards(cardsInGame, !player->isAi());
    }

    return guessRight;
}

void MainWindow::doClaimRank(Player * player, QVector<GraphicsCardItem *> cards, int rank)
{
    // Add selected cards to the game, set the new rank.
    setCurrentRank(rank);
    player->removeCards(cards, CARD_STATE_IN_GAME, false);
    ui->textBrowserLog->append("Player " + QString::number(player->number() + 1) + " claimed rank " + rank2str(rank));
    nextTurn(true);
}

void MainWindow::doBelieve(Player * player)
{
    // Open the cards in game, check their ranks.
    openCardsInGame();
    bool guessedRight = doCheck(player, DECISION_CODE_BELIEVE);

    // Increase counters.
    int prev = previousPlayerIndex(fCurrentPlayerIndex);
    if (guessedRight) {
        fCountersOfTruth[prev]++;
    } else {
        fCountersOfBullshit[prev]++;
    }

    nextRound(!guessedRight);
}

void MainWindow::doBullshit(Player * player)
{
    // Open the cards in game, check their ranks.
    openCardsInGame();
    bool guessedRight = doCheck(player, DECISION_CODE_BULLSHIT);

    // Increase counters.
    int prev = previousPlayerIndex(fCurrentPlayerIndex);
    if (guessedRight) {
        fCountersOfBullshit[prev]++;
    } else {
        fCountersOfTruth[prev]++;
    }

    // Start the next round.
    nextRound(!guessedRight);
}

void MainWindow::doSkip(Player * player)
{
    ui->textBrowserLog->append("Player " + QString::number(player->number() + 1) + " skipped his turn");
    nextRound(true);
}

QString MainWindow::rank2str(int rank)
{
    if (rank <= 10) {
        return QString::number(rank);
    } else if (rank == 11) {
        return "J";
    } else if (rank == 12) {
        return "Q";
    } else if (rank == 13) {
        return "K";
    } else {
        return "A";
    }
}

void MainWindow::setCurrentRank(int rank)
{
    fCurrentRank = rank;
    ui->labCurrentRankValue->setText(rank2str(rank));
}

void MainWindow::nextTurn(bool goToNextPlayer)
{
    // Turn-specific players state update.
    if (fCurrentPlayerIndex >= 0) {
        int tmpCurPlayer = previousPlayerIndex(fCurrentPlayerIndex);
        int distance = 1;
        while (tmpCurPlayer != -1 && tmpCurPlayer != fCurrentPlayerIndex) {
            fPlayers[tmpCurPlayer]->setIsOut(fPlayers[tmpCurPlayer]->cards().isEmpty() && distance > 1);
            tmpCurPlayer = previousPlayerIndex(tmpCurPlayer);
            distance++;
        }
    }

    // Increase turn number, go to the next player if needed.
    fCurrentTurn++;
    if (goToNextPlayer) {
        fCurrentPlayerIndex = nextPlayerIndex(fCurrentPlayerIndex);
    }

    // Enable controls and check if the game is ended.
    enableControls();
    if (fCurrentPlayerIndex == -1) {
        setCardsPositions();
        gameEnded(true);
        return;
    }

    // The player's turn itself.
    Player* player = fPlayers[fCurrentPlayerIndex];
    if (player->isAi()) {
        // This is AI turn.
        DecisionCode decision;
        QVector<GraphicsCardItem *> cardsToPlay;
        int newRank;

        QVector<GraphicsCardItem*> cardsInGame = getCardsByState(CARD_STATE_IN_GAME);
        QVector<GraphicsCardItem*> cardsOut = getCardsByState(CARD_STATE_OUT);

        // Call the AI library.
        player->decide(fDecideFuncPtr, getActivePlayers(true).size(), player->cards(), cardsOut,
                       fCurrentRank, cardsInGame.size(), 0, 0, fCountersOfTruth[player->number()], fCountersOfBullshit[player->number()],
                       &decision, &cardsToPlay, &newRank);

        // Handle the decision.
        switch (decision) {
        case DECISION_CODE_BULLSHIT:
            doBullshit(player);
            break;
        case DECISION_CODE_BELIEVE:
            doBelieve(player);
            break;
        case DECISION_CODE_ADD_CARDS:
            doClaimRank(player, cardsToPlay, newRank);
            break;
        default:
            doSkip(player);
            break;
        }
    }
    setCardsPositions();
}

void MainWindow::nextRound(bool goToNextPlayer)
{
    // Round-specific players state update.
    foreach (Player* player, fPlayers) {
        player->setIsOut(player->cards().isEmpty());
    }

    // Start the round.
    if (getActivePlayers(true).size() < 2) {
        gameEnded(true);
    } else {
        fCurrentTurn = 0;
        nextTurn(goToNextPlayer);
    }
}

void MainWindow::gameEnded(bool alert)
{
    ui->lwRank->setCurrentRow(-1);
    setCurrentRank(0);

    // Make all players active, remove all cards.
    QVector<Player*> active = getActivePlayers(true);
    foreach (Player* player, fPlayers) {
        player->setIsOut(false);
        player->clearCards();
    }
    // Reset current player index and bullshit counters.
    fCurrentPlayerIndex = -1;
    fCountersOfTruth.clear();
    fCountersOfBullshit.clear();
    foreach (Player* player, fPlayers) {
        fCountersOfTruth.insert(player->number(), 0);
        fCountersOfBullshit.insert(player->number(), 0);
    }
    enableControls();
    if (alert) {
        QString msg = "POAS rules.";
        if (!active.isEmpty()) {
            msg = "Player " + QString::number(active[0]->number() + 1) + " lost the game.";
        }
        msg += " Wanna play one more time?";
        if (QMessageBox::information(NULL, "Game ended", msg, QMessageBox::Ok, QMessageBox::Cancel) == QMessageBox::Ok) {
            actionNewGame_triggered();
        }
    }
}

void MainWindow::enableControls()
{
    bool enabled = (fCurrentPlayerIndex == getHumanPlayer()->number());
    ui->lwRank->setEnabled(enabled);
    ui->btnClaimRank->setEnabled(enabled);
    ui->btnBelieve->setEnabled(enabled && fCurrentTurn > 1);
    ui->btnBullshit->setEnabled(enabled && fCurrentTurn > 1);
}
