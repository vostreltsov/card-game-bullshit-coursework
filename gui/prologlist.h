#ifndef PROLOGLIST_H
#define PROLOGLIST_H

#include <QVector>

template<class T>
class PrologList
{
public:
    unsigned int type;      // Equals 1 if this is a regular node, 2 for the end node.
    T value;                // Value of this node.
    PrologList<T>* next;    // Pointer to the next node.

    PrologList(quint8 type, T value, PrologList<T>* next)
    {
        this->type = type;
        this->value = value;
        this->next = next;
    }

    ~PrologList()
    {
        if (type != 2 && next != NULL) {
            delete next;
        }
    }

    /**
     * Creates an empty list.
     */
    static PrologList<T>* createEmptyList()
    {
        PrologList<T>* head = new PrologList<T>(2, T(), NULL);
        return head;
    }

    /**
     * Creates a list from a vector elements.
     */
    static PrologList<T>* fromVector(QVector<T> vector)
    {
        PrologList<T>* result = createEmptyList();
        foreach (T item, vector) {
            result = result->add(item);
        }
        return result;
    }

    /**
     * Adds an item to the head of this list.
     */
    PrologList<T>* add(T value)
    {
        PrologList<T>* newHead = new PrologList<T>(1, value, this);
        return newHead;
    }

    /**
     * Converts this list to a vector.
     */
    QVector<T> toVector() const
    {
        QVector<T> result;
        PrologList<T> cur = this;
        while (cur.fType != 2) {
            result.append(cur.fValue);
            cur = cur.fNext;
        }
        return result;
    }
};

#endif // PROLOGLIST_H
