#ifndef PLAYER_H
#define PLAYER_H

#include <QVector>
#include "graphicscarditem.h"
#include "datatypes.h"

class GraphicsCardItem;

class Player
{
private:
    int fNumber;
    bool fIsAi;
    bool fIsOut;
    QVector<GraphicsCardItem *> fCards;

public:
    Player();
    Player(int number, bool isAi);
    int number() const;
    bool isAi() const;
    bool isOut() const;
    QVector<GraphicsCardItem *> cards() const;
    QVector<GraphicsCardItem *> selectedCards() const;
    void setIsOut(bool value);

    void addCard(GraphicsCardItem* card, bool open);
    void addCards(QVector<GraphicsCardItem*> cards, bool open);
    void removeCard(GraphicsCardItem* card, CardState newCardState, bool open);
    void removeCards(QVector<GraphicsCardItem*> cards, CardState newCardsStates, bool open);
    void clearCards();

    /**
     * @brief [in] decide - the main AI function that delegates the decision to the functions from dll.
     * @param [in] decideFuncPtr - pointer to the function to call.
     * @param [in] numOfActivePlayers - number of active players.
     * @param [in] cardsOnHands - list of cards on computer's hands.
     * @param [in] cardsOut - list of cards which are out.
     * @param [in] curRank - current claimed rank.
     * @param [in] numOfCards - number of cards in game.
     * @param [in] numOfCardsPrev - number of cards in game from the previous player.
     * @param [in] numOfCardsNext - number of cards in game from the next player.
     * @param [in] prevTruthCount - how many times the previous player told the truth.
     * @param [in] prevBullshitCount - how many times the previous player lied.
     * @param [out] DecisionCode - decision code (0 - don't believe, 1 - believe, 2 - believe and add cards).
     * @param [out] cardsToPlay - list of selected cards to be played.
     * @param [out] newRank - new claimed rank.
     */
    void decide(/*in*/ ai_lib_decide decideFuncPtr, int numOfActivePlayers, QVector<GraphicsCardItem*> cardsOnHands,
                QVector<GraphicsCardItem*> cardsOut, int curRank, int numOfCards, int numOfCardsPrev, int numOfCardsNext, int prevTruthCount,
                int prevBullshitCount, /*out*/ DecisionCode* DecisionCode, QVector<GraphicsCardItem*>* cardsToPlay, int* newRank) const;

private:
    static GraphicsCardItem* findGraphicsByCard(QVector<GraphicsCardItem*> cards, Card* card);
    void sortCards();

};

#endif // PLAYER_H
