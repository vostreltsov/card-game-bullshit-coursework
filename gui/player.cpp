#include "player.h"

Player::Player()
{
    Player(-1, false);
}

Player::Player(int number, bool isAi)
{
    fNumber = number;
    fIsAi = isAi;
    fIsOut = false;
}

int Player::number() const
{
    return fNumber;
}

bool Player::isAi() const
{
    return fIsAi;
}

bool Player::isOut() const
{
    return fIsOut;
}

QVector<GraphicsCardItem *> Player::cards() const
{
    return fCards;
}

QVector<GraphicsCardItem *> Player::selectedCards() const
{
    QVector<GraphicsCardItem *> result;
    foreach (GraphicsCardItem* card, fCards) {
        if (card->isSelected()) {
            result.append(card);
        }
    }
    return result;
}

void Player::setIsOut(bool value)
{
    fIsOut = value;
}

void Player::addCard(GraphicsCardItem* card, bool open)
{
    card->setClosed(!open);
    card->card()->owner = fNumber;
    card->card()->state = CARD_STATE_ON_HANDS;
    fCards.append(card);
    sortCards();
}

void Player::addCards(QVector<GraphicsCardItem*> cards, bool open)
{
    foreach (GraphicsCardItem * card, cards) {
        addCard(card, open);
    }
}

void Player::removeCard(GraphicsCardItem* card, CardState newCardState, bool open)
{
    for (int i = 0; i < fCards.size(); i++) {
        if (fCards[i] == card) {
            card->card()->owner = -1;
            card->card()->state = newCardState;
            card->setClosed(!open);
            fCards.remove(i);
            sortCards();
            return;
        }
    }
}

void Player::removeCards(QVector<GraphicsCardItem*> cards, CardState newCardsStates, bool open)
{
    foreach (GraphicsCardItem * card, cards) {
        removeCard(card, newCardsStates, open);
    }
}

void Player::clearCards()
{
    fCards.clear();
}

void Player::decide(ai_lib_decide decideFuncPtr, int numOfActivePlayers, QVector<GraphicsCardItem*> cardsOnHands,
                    QVector<GraphicsCardItem*> cardsOut, int curRank, int numOfCards, int numOfCardsPrev, int numOfCardsNext, int prevTruthCount,
                    int prevBullshitCount, DecisionCode* decisionCode, QVector<GraphicsCardItem*>* cardsToPlay, int* newRank) const
{
    // Create a list of cards to pass to dll.
    PrologList<Card *>* list1 = PrologList<Card *>::createEmptyList();
    foreach (GraphicsCardItem* card, cardsOnHands) {
        list1 = list1->add(card->card());
    }
    PrologList<Card *>* list2 = PrologList<Card *>::createEmptyList();
    foreach (GraphicsCardItem* card, cardsOut) {
        list2 = list2->add(card->card());
    }

    PrologList<Card *>* listToPlay;

    // Call the AI library.
    decideFuncPtr(numOfActivePlayers, list1, list2, curRank, numOfCards, numOfCardsPrev, numOfCardsNext, prevTruthCount, prevBullshitCount,
                  decisionCode, &listToPlay, newRank);

    // Update the cardsToPlay vector.
    cardsToPlay->clear();
    if (*decisionCode == DECISION_CODE_ADD_CARDS) {
        PrologList<Card *>* curCard = listToPlay;
        while (curCard->type != 2) {
            GraphicsCardItem * graphicsCard = findGraphicsByCard(cardsOnHands, curCard->value);
            if (graphicsCard != NULL) {
                graphicsCard->card()->state = CARD_STATE_IN_GAME;    // Just in case.
                graphicsCard->card()->owner = -1;
                cardsToPlay->append(graphicsCard);
            }
            curCard = curCard->next;
        }
    }
}

GraphicsCardItem* Player::findGraphicsByCard(QVector<GraphicsCardItem*> cards, Card* card)
{
    foreach (GraphicsCardItem* result, cards) {
        if (*(result->card()) == *card) {
            return result;
        }
    }
    return NULL;
}

void Player::sortCards()
{
    for (int i = 0; i < fCards.size(); i++) {
        for (int j = i + 1; j < fCards.size(); j++) {
            if (fCards[i]->card()->rank < fCards[j]->card()->rank) {
                GraphicsCardItem* tmp = fCards[i];
                fCards[i] = fCards[j];
                fCards[j] = tmp;
            }
        }
    }
}
