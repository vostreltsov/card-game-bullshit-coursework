include "ai.inc"

global domains
    state = integer % in game (0), on hands (1), out (2).
    suit = integer % spades (0), hearts (1), diamonds (2), clubs (3).
    card = card_struct(state, suit, integer, integer) % state, suit, rank, owner.
    list_card = card*

global predicates

    % Makes a decision for the current game situation.
    % [in]  Number of active players.
    % [in]  List of cards on computer's hands.
    % [in]  List of cards which are out.
    % [in]  Current claimed rank.
    % [in]  Number of cards in game.
    % [in]  Number of cards in game from the previous player.
    % [in]  Number of cards in game from the next player.
    % [in]  How many times the previous player told the truth.
    % [in]  How many times the previous player lied.
    % [out] Decision code (0 - don't believe, 1 - believe, 2 - believe and add cards).
    % [out] List of selected cards to be played.
    % [out] New claimed rank.
    procedure decide(integer, list_card, list_card, integer, integer, integer, integer, integer, integer, integer, list_card, integer) - (i, i, i, i, i, i, i, i, i, o, o, o) language stdcall

    % Performs a default decision.
    % [in]  Randomly choosen decision code.
    % [in]  List of cards on computer's hands.
    % [in]  Current claimed rank.
    % [out] List of selected cards to be played.
    % [out] New claimed rank.
    procedure default_decision(integer, list_card, integer, integer, list_card, integer) - (i, i, i, o, o, o) language stdcall

    % Add cards to the game when there's no cards yet.
    % [in]  List of cards on computer's hands.
    % [in]  Rank of which max number of cards exist in the list.
    % [in]  Number of cards (of that rank) to add.
    % [out] Decision code.
    % [out] List of selected cards to be played.
    % [out] New claimed rank.
    procedure add_cards_to_game_first(list_card, integer, integer, integer, list_card, integer) - (i, i, i, o, o, o) language stdcall

    % Returns the rank of the first card in a list
    % [in]  List of cards.
    % [out] Rank of the first card.
    procedure get_rank_of_first_card(list_card, integer) - (i, o) language stdcall

    % Forms a list of randomly selected cards.
    % [in]  List of available cards.
    % [in]  Needed count of random cards.
    % [in]  Current count of random cards.
    % [out] Resulting list of randomly selected cards.
    procedure get_random_cards(list_card, integer, integer, list_card) - (i, i, i, o) language stdcall

    % Counds number of cards in the given list.
    % [in]  The input list.
    % [out] Length of the list.
    procedure number_of_cards(list_card, integer) - (i, o) language stdcall

    % Counds number of cards of the specified rank in the given list.
    % [in] List of cards to search in.
    % [in] Needed rank.
    % [out] Resulting value.
    procedure number_of_cards_of_rank(list_card, integer, integer) - (i, i, o) language stdcall

    % Returns the rank with max number of cards in a list.
    % [in] List of cards to search in.
    % [in] Current rank.
    % [out] Resulting rank.
    % [out] Resulting count of cards of the rank.
    procedure rank_of_max_count(list_card, integer, integer, integer) - (i, i, o, o) language stdcall

    % Returns a rank with more number of cards.
    % [in] Rank 1 to compare.
    % [in] Number of cards of rank 1.
    % [in] Rank 2 to compare.
    % [in] Number of cards of rank 2.
    % [out] Resulting rank.
    % [out] Resulting count of cards of the rank.
    procedure compare_ranks_max(integer, integer, integer, integer, integer, integer) - (i, i, i, i, o, o) language stdcall

    % Returns a sublist of cards of the specified rank.
    % [in] List of cards to take resulting cards from.
    % [in] Needed rank.
    % [out] Resulting list of cards.
    procedure get_cards_by_rank(list_card, integer, list_card) - (i, i, o) language stdcall

    % Returns a card by its index in a list.
    % [in] List of cards to take resulting card from.
    % [in] Index of the card.
    % [in] Current index.
    % [out] Resulting card.
    procedure get_card_by_index(list_card, integer, integer, card) - (i, i, i, o) language stdcall

    % Removes a card from the list.
    % [in] Original list of cards.
    % [in] Index of the card to remove.
    % [in] Current index.
    % [out] Resulting list of cards.
    procedure remove_card_by_index(list_card, integer, integer, list_card) - (i, i, i, o) language stdcall

    % Concatenates 2 lists.
    % [in] First list.
    % [in] Second list.
    % [out] Concatenation of the lists.
    procedure concatenate_lists(list_card, list_card, list_card) - (i, i, o) language stdcall

constants
    decision_code_bullshit = 0
    decision_code_believe = 1
    decision_code_add_cards = 2

clauses

    % strategy 1. No cards in game - add new cards.
    decide(_, CardsOnHands, _, _, 0, _, _, _, _, DecisionCode, CardsToPlay, NewRank) :-
        rank_of_max_count(CardsOnHands, 0, RankOfMaxCount, Count),
        NumberOfRemainingCards = 4 - Count + 1,
        random(NumberOfRemainingCards, NumberOfRandomCards),    % from 0 to (4 - Count)

        add_cards_to_game_first(CardsOnHands, RankOfMaxCount, Count, DecisionCode, CardsToPlay1, NewRank),
        get_random_cards(CardsOnHands, NumberOfRandomCards, 0, CardsToPlay2),
        concatenate_lists(CardsToPlay1, CardsToPlay2, CardsToPlay),
        !.

    % strategy 2. More than 4 cards in game - bullshit.
    decide(_, _, _, CurRank, NumOfCards, _, _, _, _, DecisionCode, CardsToPlay, NewRank) :-
        NumOfCards > 4,

        DecisionCode = decision_code_bullshit,
        CardsToPlay = [],
        NewRank = CurRank,
        !.

    % strategy 3. Number of cards in game + number of out cards of the current rank > 4 - bullshit.
    decide(_, _, OutCards, CurRank, NumOfCards, _, _, _, _, DecisionCode, CardsToPlay, NewRank) :-
        number_of_cards_of_rank(OutCards, CurRank, Length),
        NumOfCards + Length > 4,

        DecisionCode = decision_code_bullshit,
        CardsToPlay = [],
        NewRank = CurRank,
        !.

    % strategy 4. Number of cards in game + number of cards on hands of the current rank <= 4, number of out cards of the current rank = 0 - add new cards.
    decide(_, CardsOnHands, OutCards, CurRank, NumOfCards, _, _, _, _, DecisionCode, CardsToPlay, NewRank) :-
        NumOfCards < 4,
        number_of_cards_of_rank(CardsOnHands, CurRank, LengthOnHands),
        LengthOnHands > 0,
        NumOfCards + LengthOnHands <= 4,
        number_of_cards_of_rank(OutCards, CurRank, LengthOut),
        LengthOut = 0,

        NumberOfRemainingCards = 4 - NumOfCards - LengthOnHands + 1,
        random(NumberOfRemainingCards, NumberOfRandomCards),    % from 0 to (4 - Count)

        DecisionCode = decision_code_add_cards,
        get_cards_by_rank(CardsOnHands, CurRank, CardsToPlay1),
        get_random_cards(CardsOnHands, NumberOfRandomCards, 0, CardsToPlay2),
        concatenate_lists(CardsToPlay1, CardsToPlay2, CardsToPlay),
        NewRank = CurRank,
        !.

    % strategy 5. Previous player lied more times than told the truth - bullshit.
    decide(_, _, _, CurRank, _, _, _, TruthCount, BullshitCount, DecisionCode, CardsToPlay, NewRank) :-
        BullshitCount > TruthCount,

        DecisionCode = decision_code_bullshit,
        CardsToPlay = [],
        NewRank = CurRank,
        !.

    % strategy 6. Number of the previous player's cards in game is 1 or 2, number of cards on hands of the same rank > 0 - bullshit.
    decide(_, CardsOnHands, _, CurRank, _, NumOfCardsPrev, _, _, _, DecisionCode, CardsToPlay, NewRank) :-
        NumOfCardsPrev >= 1,
        NumOfCardsPrev <= 2,
        number_of_cards_of_rank(CardsOnHands, CurRank, Length),
        Length > 0,

        DecisionCode = decision_code_bullshit,
        CardsToPlay = [],
        NewRank = CurRank,
        !.

    % strategy 7. Number of the next player's cards in game is 1 or 2, number of cards on hands of the same rank > 0 - bullshit.
    decide(_, CardsOnHands, _, CurRank, _, _, NumOfCardsNext, _, _, DecisionCode, CardsToPlay, NewRank) :-
        NumOfCardsNext >= 1,
        NumOfCardsNext <= 2,
        number_of_cards_of_rank(CardsOnHands, CurRank, Length),
        Length > 0,

        DecisionCode = decision_code_bullshit,
        CardsToPlay = [],
        NewRank = CurRank,
        !.

    % strategy 8. 3 cards in game, 1 card of the current rank on hands - bullshit.
    decide(_, CardsOnHands, _, CurRank, NumOfCards, _, _, _, _, DecisionCode, CardsToPlay, NewRank) :-
        NumOfCards = 3,
        number_of_cards_of_rank(CardsOnHands, CurRank, Length),
        Length > 0,

        DecisionCode = decision_code_bullshit,
        CardsToPlay = [],
        NewRank = CurRank,
        !.

    % strategy 9. Number of cards in game is 1...2, number of cards of the same rank on hands is 1..2 - believe.
    decide(_, CardsOnHands, _, CurRank, NumOfCards, _, _, _, _, DecisionCode, CardsToPlay, NewRank) :-
        NumOfCards >= 1,
        NumOfCards <= 2,
        number_of_cards_of_rank(CardsOnHands, CurRank, Length),
        Length >= 1,
        Length <= 2,

        DecisionCode = decision_code_believe,
        CardsToPlay = [],
        NewRank = CurRank,
        !.

    % strategy 10. 2 players, many cards on hands, 1 card from the previous player - bullshit.
    decide(2, CardsOnHands, _, CurRank, _, NumOfCardsPrev, _, _, _, DecisionCode, CardsToPlay, NewRank) :-
        NumOfCardsPrev = 1,
        number_of_cards_of_rank(CardsOnHands, CurRank, Length),
        Length > 5,

        DecisionCode = decision_code_bullshit,
        CardsToPlay = [],
        NewRank = CurRank,
        !.

    % strategy 11. Number of cards in game < 3, number of out cards of the current rank = 0 - add new random cards.
    decide(_, CardsOnHands, OutCards, CurRank, NumOfCards, _, _, _, _, DecisionCode, CardsToPlay, NewRank) :-
        NumOfCards < 3,
        number_of_cards_of_rank(OutCards, CurRank, LengthOut),
        LengthOut = 0,
        NumberOfRandomCards = 4 - NumOfCards,

        DecisionCode = decision_code_add_cards,
        get_random_cards(CardsOnHands, NumberOfRandomCards, 0, CardsToPlay),
        NewRank = CurRank,
        !.

    % strategy 12. 2 players, no cards of the current rank on hands - believe.
    decide(2, CardsOnHands, _, CurRank, _, _, _, _, _, DecisionCode, CardsToPlay, NewRank) :-
        number_of_cards_of_rank(CardsOnHands, CurRank, Length),
        Length = 0,

        DecisionCode = decision_code_believe,
        CardsToPlay = [],
        NewRank = CurRank,
        !.

    % strategy 13. 2 players, no cards from the next player, 1 or 2 cards from the previous player - believe.
    decide(_, _, _, CurRank, _, NumOfCardsPrev, 0, _, _, DecisionCode, CardsToPlay, NewRank) :-
        NumOfCardsPrev >= 1,
        NumOfCardsPrev <= 2,

        DecisionCode = decision_code_believe,
        CardsToPlay = [],
        NewRank = CurRank,
        !.

    % strategy 14. Number of cards of the current rank on hands + number of cards in game < 4 - believe.
    decide(_, CardsOnHands, _, CurRank, NumOfCards, _, _, _, _, DecisionCode, CardsToPlay, NewRank) :-
        number_of_cards_of_rank(CardsOnHands, CurRank, Length),
        NumOfCards + Length < 4,

        DecisionCode = decision_code_believe,
        CardsToPlay = [],
        NewRank = CurRank,
        !.

    % strategy 15. Some cards are out, many cards on hands - believe.
    decide(_, CardsOnHands, OutCards, CurRank, _, _, _, _, _, DecisionCode, CardsToPlay, NewRank) :-
        number_of_cards(OutCards, LengthOut),
        LengthOut > 0,
        number_of_cards(CardsOnHands, Length),
        Length > 5,

        DecisionCode = decision_code_believe,
        CardsToPlay = [],
        NewRank = CurRank,
        !.

    % strategy 16. More than 2 players, few cards on hands - believe.
    decide(NumOfPlayers, CardsOnHands, _, CurRank, _, _, _, _, _, DecisionCode, CardsToPlay, NewRank) :-
        NumOfPlayers > 2,
        number_of_cards(CardsOnHands, Length),
        Length < 5,

        DecisionCode = decision_code_believe,
        CardsToPlay = [],
        NewRank = CurRank,
        !.

    % strategy 17. 2 players, few cards on hands, 1...3 card in game - believe.
    decide(2, CardsOnHands, _, CurRank, NumOfCards, _, _, _, _, DecisionCode, CardsToPlay, NewRank) :-
        NumOfCards < 4,
        number_of_cards(CardsOnHands, Length),
        Length < 5,

        DecisionCode = decision_code_believe,
        CardsToPlay = [],
        NewRank = CurRank,
        !.

    % strategy 18. 2 players, 1 or 2 cards of the current rank on hands - believe.
    decide(2, CardsOnHands, _, CurRank, _, _, _, _, _, DecisionCode, CardsToPlay, NewRank) :-
        number_of_cards_of_rank(CardsOnHands, CurRank, Length),
        Length >= 1,
        Length <= 2,

        DecisionCode = decision_code_believe,
        CardsToPlay = [],
        NewRank = CurRank,
        !.

    % strategy 19. 2 players, few cards on hands, less than 3 cards in game from the previous player - believe.
    decide(2, CardsOnHands, _, CurRank, _, NumOfCardsPrev, _, _, _, DecisionCode, CardsToPlay, NewRank) :-
        NumOfCardsPrev < 3,
        number_of_cards(CardsOnHands, Length),
        Length < 5,

        DecisionCode = decision_code_believe,
        CardsToPlay = [],
        NewRank = CurRank,
        !.

    % Default strategy 20.1. Decide probabilistically. Number of cards in game < 4, all 3 decisions are available.
    decide(_, CardsOnHands, _, CurRank, NumOfCards, _, _, _, _, DecisionCode, CardsToPlay, NewRank) :-
        NumOfCards < 4,
        number_of_cards_of_rank(CardsOnHands, CurRank, NumOfCardsThis),
        NumOfCardsThis > 0,
        random(3, GodsDecision),
        default_decision(GodsDecision, CardsOnHands, CurRank, DecisionCode, CardsToPlay, NewRank),
        !.

    % Default strategy 20.2. Decide probabilistically. Number of cards in game > 3, only believe/bullshit available.
    decide(_, CardsOnHands, _, CurRank, _, _, _, _, _, DecisionCode, CardsToPlay, NewRank) :-
        random(2, GodsDecision),
        default_decision(GodsDecision, CardsOnHands, CurRank, DecisionCode, CardsToPlay, NewRank),
        !.

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    default_decision(decision_code_add_cards, CardsOnHands, CurRank, DecisionCode, CardsToPlay, NewRank) :-
        DecisionCode = decision_code_add_cards,
        get_cards_by_rank(CardsOnHands, CurRank, CardsToPlay),
        NewRank = CurRank,
        !.

    default_decision(BelieveOrBullshit, _, CurRank, DecisionCode, CardsToPlay, NewRank) :-
        DecisionCode = BelieveOrBullshit,
        CardsToPlay = [],
        NewRank = CurRank,
        !.

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    add_cards_to_game_first(CardsOnHands, _, 1, DecisionCode, CardsToPlay, NewRank) :-
        % No cards repetitions, throw some random cards.
        DecisionCode = decision_code_add_cards,
        % Let Lord decide how many cards we will throw.
        random(4, ZeroBasedNumberOfRandomCards), % from 0 to 3
        NumberOfRandomCards = 1 + ZeroBasedNumberOfRandomCards, % from 1 to 4
        get_random_cards(CardsOnHands, NumberOfRandomCards, 0, CardsToPlay),
        get_rank_of_first_card(CardsToPlay, NewRank),
        !.

    add_cards_to_game_first(CardsOnHands, RankOfMaxCount, _, DecisionCode, CardsToPlay, NewRank) :-
        % Cards of the rank RankOfMaxCount are repeated MaxCount times, throw them all.
        DecisionCode = decision_code_add_cards,
        get_cards_by_rank(CardsOnHands, RankOfMaxCount, CardsToPlay),
        NewRank = RankOfMaxCount,
        !.

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    get_rank_of_first_card([card_struct(_, _, Rank, _)|_], OutRank) :-
        OutRank = Rank,
        !.

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    get_random_cards([], _, _, Result) :-
        Result = [],
        !.

    get_random_cards(_, NeededCount, NeededCount, Result) :-
        Result = [],
        !.

    get_random_cards(AvailableCards, NeededCount, CurrentCount, Result) :-
        number_of_cards(AvailableCards, Length),
        random(Length, RandomIndex),
        get_card_by_index(AvailableCards, RandomIndex, 0, RandomCard),
        remove_card_by_index(AvailableCards, RandomIndex, 0, NewAvailableCards),
        NewCount = CurrentCount + 1,
        get_random_cards(NewAvailableCards, NeededCount, NewCount, ResultInner),
        Result = [RandomCard|ResultInner],
        !.

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    number_of_cards([], Len) :-
        Len = 0,
        !.

    number_of_cards([_|Tail], Len) :-
        number_of_cards(Tail, LenInner),
        Len = LenInner + 1,
        !.

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    number_of_cards_of_rank([], _, Result) :-
        Result = 0,
        !.

    number_of_cards_of_rank([card_struct(_, _, Rank, _)|TCards], Rank, Result) :-
        number_of_cards_of_rank(TCards, Rank, ResultInner),
        Result = ResultInner + 1,
        !.

    number_of_cards_of_rank([_|TCards], Rank, Result) :-
        number_of_cards_of_rank(TCards, Rank, ResultInner),
        Result = ResultInner,
        !.

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    rank_of_max_count(_, 15, ResultRank, ResultCount) :-
        ResultRank = 0,
        ResultCount = 0,
        !.

    rank_of_max_count(Cards, CurRank, ResultRank, ResultCount) :-
        NextRank = CurRank + 1,
        rank_of_max_count(Cards, NextRank, ResultRankInner, ResultCountInner),
        number_of_cards_of_rank(Cards, CurRank, CurCount),
        compare_ranks_max(CurRank, CurCount, ResultRankInner, ResultCountInner, ResultRank, ResultCount),
        !.

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    compare_ranks_max(Rank1, Rank1Count, _, Rank2Count, RankMax, RankMaxCount) :-
        Rank1Count > Rank2Count,
        RankMax = Rank1,
        RankMaxCount = Rank1Count,
        !.

    compare_ranks_max(_, _, Rank2, Rank2Count, RankMax, RankMaxCount) :-
        RankMax = Rank2,
        RankMaxCount = Rank2Count,
        !.

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    get_cards_by_rank([], _, Result) :-
        Result = [],
        !.

    get_cards_by_rank([card_struct(State, Suit, Rank, Owner)|TCards], Rank, Result) :-
        get_cards_by_rank(TCards, Rank, ResultInner),
        Result = [card_struct(State, Suit, Rank, Owner)|ResultInner],
        !.

    get_cards_by_rank([_|TCards], Rank, Result) :-
        get_cards_by_rank(TCards, Rank, Result),
        !.

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    get_card_by_index([], _, _, Result) :-
        Result = card_struct(-1, -1, -1, -1),
        !.

    get_card_by_index([HCard|_], NeededIndex, NeededIndex, Result) :-
        Result = HCard,
        !.

    get_card_by_index([_|TCards], NeededIndex, CurIndex, Result) :-
        NewIndex = CurIndex + 1,
        get_card_by_index(TCards, NeededIndex, NewIndex, Result),
        !.

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    remove_card_by_index([], _, _, Result) :-
        Result = [],
        !.

    remove_card_by_index([_|Tail], IndexToRemove, IndexToRemove, Result) :-
        NextIndex = IndexToRemove + 1,
        remove_card_by_index(Tail, IndexToRemove, NextIndex, ResultInner),
        Result = ResultInner,
        !.

    remove_card_by_index([Head|Tail], IndexToRemove, CurIndex, Result) :-
        NextIndex = CurIndex + 1,
        remove_card_by_index(Tail, IndexToRemove, NextIndex, ResultInner),
        Result = [Head|ResultInner],
        !.

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    concatenate_lists([], L, L).

    concatenate_lists([N|L1], L2, [N|L3]) :-
        concatenate_lists(L1, L2, L3).

goal
    !.
