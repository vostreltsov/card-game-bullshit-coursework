#ifndef DATATYPES_H
#define DATATYPES_H

#include "prologlist.h"

enum CardSuit
{
    CARD_SUIT_SPADES,
    CARD_SUIT_HEARTS,
    CARD_SUIT_DIAMONDS,
    CARD_SUIT_CLUBS
};

enum CardState
{
    CARD_STATE_IN_GAME,
    CARD_STATE_ON_HANDS,
    CARD_STATE_OUT
};

struct Card
{
    unsigned char type;
    CardState state;
    CardSuit suit;
    int rank;
    int owner;
    Card(CardState _state, CardSuit _suit, int _rank, int _owner)
    {
        type = 1;
        state = _state;
        suit = _suit;
        rank = _rank;
        owner = _owner;
    }
    bool operator==(const Card & other) const
    {
        return (suit == other.suit && rank == other.rank);
    }
};

enum DecisionCode
{
    DECISION_CODE_BULLSHIT,
    DECISION_CODE_BELIEVE,
    DECISION_CODE_ADD_CARDS
};

// [in]  number of active players.
// [in]  list of cards on computer's hands.
// [in]  list of cards which are out.
// [in]  current claimed rank.
// [in]  number of cards in game.
// [in]  number of cards in game from the previous player.
// [in]  number of cards in game from the next player.
// [in]  how many times the previous player told the truth.
// [in]  how many times the previous player lied.
// [out] decision code (0 - don't believe, 1 - believe, 2 - believe and add cards).
// [out] list of selected cards to be played.
// [out] new claimed rank.
typedef int (__stdcall *ai_lib_decide) (int, PrologList<Card*> *, PrologList<Card*> *, int, int, int, int, int, int, DecisionCode*, PrologList<Card*>**, int*);

#endif // DATATYPES_H
