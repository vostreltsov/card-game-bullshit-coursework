#-------------------------------------------------
#
# Project created by QtCreator 2012-12-23T21:15:00
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = liar
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    player.cpp \
    graphicscarditem.cpp

HEADERS  += mainwindow.h \
    prologlist.h \
    player.h \
    datatypes.h \
    graphicscarditem.h

FORMS    += mainwindow.ui

RESOURCES += \
    resources.qrc
