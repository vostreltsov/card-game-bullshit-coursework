#ifndef GRAPHICSCARDITEM_H
#define GRAPHICSCARDITEM_H

#include <QGraphicsPixmapItem>
#include "datatypes.h"
#include "mainwindow.h"

class MainWindow;

class GraphicsCardItem : public QGraphicsPixmapItem
{
private:
    MainWindow* fMainWindow;
    Card* fCard;
    QPixmap fPixmapFront;
    QPixmap fPixmapBack;
    bool fIsSelected;
    bool fIsClosed;

public:
    GraphicsCardItem();
    GraphicsCardItem(MainWindow* mainWindow, Card * card, QPixmap pixmapFront, QPixmap pixmapBack);
    ~GraphicsCardItem();
    Card* card() const;
    bool isSelected() const;
    bool isClosed() const;
    void setSelected(bool selected);
    void setClosed(bool closed);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
};

#endif // GRAPHICSCARDITEM_H
