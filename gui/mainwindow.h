#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QMessageBox>
#include <QLibrary>
#include <QVector>
#include <QMap>
#include <QResource>
#include <QImage>

#include "datatypes.h"
#include "prologlist.h"
#include "player.h"
#include "graphicscarditem.h"

namespace Ui {
class MainWindow;
}

class Player;
class GraphicsCardItem;

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QLibrary* aiLib() const;

    void onGraphicsCardItemClicked(GraphicsCardItem * item);

private slots:
    void actionNewGame_triggered();
    void actionExit_triggered();
    void actionAbout_triggered();
    void btnClaimRank_clicked();
    void btnBelieve_clicked();
    void btnBullshit_clicked();
    
private:
    static const int NUMBER_OF_PLAYERS = 4;        // Number of players,
    static const int RANK_MIN = 6;                 // Min card rank.
    static const int RANK_MAX = 14;                // Max card rank.
    static const int POSITION_SCENE_WIDTH = 800;   // Width of the scene.
    static const int POSITION_SCENE_HEIGHT = 400;  // Height of the scene.
    static const int POSITION_SELECTED_DELTA = 18; // Number of pixels betweed cards.
    static const int POSITION_X_LEFT = 10;
    static const int POSITION_Y_TOP = 10;          // Y coordinate at which top opponent's cards are drawing.
    static const int POSITION_Y_BOTTOM = 300;      // Y coordinate at which human cards are drawing.

    Ui::MainWindow *ui;                      // Main window.
    QGraphicsScene *fScene;                  // Scene for drawing the images.
    QLibrary * fAiLib;                       // AI library loaded dynamically.
    ai_lib_decide fDecideFuncPtr;            // Pointer to the decision predicate.
    QVector<Player *> fPlayers;              // Players.
    QVector<GraphicsCardItem*> fCards;       // Set of cards.
    QMap<int, int> fCountersOfTruth;         // How many times each player told the truth?
    QMap<int, int> fCountersOfBullshit;      // How many times each player lied?

    int fCurrentPlayerIndex;                 // Index of the current player.
    int fCurrentRank;                        // Current claimed rank.
    int fCurrentTurn;                        // Current turn number, reset to 0 every round.

    // Functions to deal with players.
    int nextPlayerIndex(int currentPlayer) const;
    int previousPlayerIndex(int currentPlayer) const;
    Player* getHumanPlayer() const;
    QVector<Player *> getAiPlayers() const;
    QVector<Player *> getActivePlayers(bool active) const;

    // Functions to deal with cards.
    QVector<GraphicsCardItem*> getCardsByState(CardState state) const;
    QVector<GraphicsCardItem*> findCardsOfRank(QVector<GraphicsCardItem *> cards, int rank) const;
    QPixmap getCardPixmap(Card* card);
    void setCardsPositions();
    void dealCards();
    void openCardsInGame();
    void throwCardsOut(QVector<GraphicsCardItem*> cards);

    // Functions to deal with decisions.
    bool doCheck(Player * player, DecisionCode decision);
    void doClaimRank(Player * player, QVector<GraphicsCardItem *> cards, int rank);
    void doBelieve(Player * player);
    void doBullshit(Player * player);
    void doSkip(Player * player);

    // Misc functions.
    QString rank2str(int rank);
    void setCurrentRank(int rank);
    void nextTurn(bool goToNextPlayer);
    void nextRound(bool goToNextPlayer);
    void gameEnded(bool alert);
    void enableControls();
};

#endif // MAINWINDOW_H
